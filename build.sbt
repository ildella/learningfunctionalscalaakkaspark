name := "Functional Programming Course in Scala"
version := "1.0"
scalaVersion := "2.10.4"

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies +=  "org.apache.logging.log4j" % "log4j-api" % "2.0"
libraryDependencies +=  "org.apache.logging.log4j" % "log4j-core" % "2.0"
libraryDependencies +=  "org.apache.logging.log4j" % "log4j-1.2-api" % "2.0"
libraryDependencies +=  "org.slf4j" % "slf4j-log4j12" % "1.7.7"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.0" % "test" withSources()

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.2.1" withSources()
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.9" withSources()
libraryDependencies += "com.typesafe.akka" %% "akka-camel" % "2.3.9" withSources()
libraryDependencies += "com.typesafe.akka" %% "akka-http-experimental" % "1.0-M4" withSources()

libraryDependencies += "org.twitter4j" % "twitter4j-core" % "4.0.2" withSources()
libraryDependencies += "org.reactivemongo" %% "reactivemongo" % "0.10.5.0.akka23"