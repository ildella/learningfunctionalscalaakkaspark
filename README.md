# Learning Functional programming, Scala, Actors with Akka and Spark #

Here is the code for my firsts hands-on with these technologies. 

### What is this repository for? ###

* code for the example (not the exercises) used in the course Functional Programming in Scala by @odersky
* code for the example (not the exercises) used in the course Principles of Reactive Programming by @odersky, Meijer and Kuhn
* more code for some personal experiments with Scala
* Spark tutorials and experiments 


### How do I get set up? ###

* Install JVM and SBT
* Checkout code
* run "sbt test" or in general, use sbt to see it working. Test suite should be actually green :)

### Contribution guidelines ###

* Nothing for now.

### Who do I talk to? ###

* Everybody who wants to start using this techs. 