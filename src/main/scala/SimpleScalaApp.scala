object SimpleScalaApp {

  def main(args: Array[String]) {
    println("Lines with a: %s, Lines with b: %s".format("1", "2"))
    println("args size %s".format(args.size))
    println("arg 0 %s, arg 1 %s".format(args{0}, args{1}))
    (1, 2)
  }
  
}