import scala.concurrent._
import scala.concurrent.duration._
import akka.actor._

object AkkaCliApp extends App {

  println("Welcome!")

  val system = ActorSystem("myHome")

  val father = system.actorOf(Props(Parent()), "andrea")
  //val mother = system.actorOf(Props(Parent()), "francesca")

  val marta = system.actorOf(Props(Baby()), "marta")
  //val teresa = system.actorOf(Props(Baby()), "teresa")
  val quit = system.actorOf(Props(Doer()), "quitter")

  val family = List(father)
  val doers = List(quit)

  object BabyMsgs {

    case object Cry
    case object Quit

  }

  case class Baby() extends Actor with SchedulerImpl {
    import BabyMsgs._

    val hungry = every(1 hours)(doCry)
    //    val dirty = every(7 hours)(doCry)

    def doCry() = {
      //      println(s"${self.path} Uheee!")
      //      family.foreach { _ ! Cry }
      val scanner = new java.util.Scanner(System.in)
      val line = scanner.nextLine()
      println(s"${self.path} received command: ${line}")
      val isCommand = line.startsWith("/")
        
    }

    def receive = {
      case _ =>
        println(s"${self.path} Uh?!")
    }
  }

  case class Parent() extends Actor {
    import BabyMsgs._

    def receive = {
      case Cry =>
        println(s"${self.path} Panic! ${sender.path} is crying!")
    }
  }

  case class Doer() extends Actor {
    import BabyMsgs._

    def receive = {
      case Quit =>
        println(s"${self.path} Ok ${sender.path}, I'll do that")
    }
  }
  
}