package akka

import scala.concurrent._
import scala.concurrent.duration._
import akka.actor._

case class Pay (amount: String)

case class Parent() extends Actor {

  def receive = {
    case _ =>
      println(s"${self.path} executing from ${sender.path}")
  }
}

object CliController {

  def main(args: Array[String]) {
    println("Hi, I am Charlie. What do you need?")

    val system = ActorSystem("CliController")
    val payment = system.actorOf(Props(Parent()), "payment")

    var condition = true

    while (condition) {
      Thread.sleep(100)
      val scanner = new java.util.Scanner(System.in)
      val line = scanner.nextLine()  
      // println("received command: " + line)
      val exitCommand = line.equals("/exit")
      condition = condition && !exitCommand
      // println("exit condition: " + condition)
      val args = line.split(" ")
      val command = args(0)
      val param = args(1)
      println("cmd " + command + " param: " + param)
      
      command match {
        case "pay" => {
          println("Got it, executing in background. Will notify here when completed. ")
          payment ! param
        }
        case _ => {
          println("Ummm, I do not know how to do that")
        }
      }
      println()
      println("Do you need something else? ")
    }
    println("*** Shutting down. See you soon.")
  }

}