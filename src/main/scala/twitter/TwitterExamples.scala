package twitter
// examples from https://bcomposes.wordpress.com/2013/02/26/using-twitter4j-with-scala-to-perform-user-actions/


import twitter4j._
import scala.collection.JavaConversions._

trait TwitterInstance {
  val twitter = new TwitterFactory().getInstance
}

object QuerySearch extends TwitterInstance {

  def main(args: Array[String]) {
    val statuses = twitter.search(new Query(args(0))).getTweets
    statuses.foreach(status => println(status.getText + "\n"))
  }

}

object GetHomeTimeline extends TwitterInstance {

  def main(args: Array[String]) {
    val num = if (args.length == 1) args(0).toInt else 10
    val statuses = twitter.getHomeTimeline.take(num)
    statuses.foreach(status => println(status.getText + "\n"))
  }

}

object UpdateStatus extends TwitterInstance {
  def main(args: Array[String]) {
    twitter.updateStatus(new StatusUpdate(args(0)))
  }
}

trait RateChecker {

  def checkAndWait(response: TwitterResponse, verbose: Boolean = false) {
    val rateLimitStatus = response.getRateLimitStatus
    if (verbose) println("RLS: " + rateLimitStatus)

    if (rateLimitStatus != null && rateLimitStatus.getRemaining == 0) {
      println("*** You hit your rate limit. ***")
      val waitTime = rateLimitStatus.getSecondsUntilReset + 10
      println("Waiting " + waitTime + " seconds ( " + waitTime / 60.0 + " minutes) for rate limit reset.")
      Thread.sleep(waitTime * 1000)
    }
  }

}

object DescribeFollowers extends TwitterInstance with RateChecker {

  def main(args: Array[String]) {
    val screenName = args(0)
    val maxUsers = if (args.length == 2) args(1).toInt else 5
    val followerIds = twitter.getFollowersIDs(screenName, -1).getIDs

    val descriptions = followerIds.take(maxUsers).flatMap { id =>
      {
        val user = twitter.showUser(id)
        checkAndWait(user)
        if (user.isProtected) None else Some(user.getDescription)
      }
    }

    val tword = """(?i)[a-z#@]+""".r.pattern
    val words = descriptions.flatMap(_.toLowerCase.split("\\s+"))
    val filtered = words.filter(_.length > 3).filter(tword.matcher(_).matches)
    val counts = filtered.groupBy(x => x).mapValues(_.length)
    val rankedCounts = counts.toSeq.sortBy(-_._2)

    import java.io._
    val wordcountFile = "/tmp/follower_wordcount.txt"
    val writer = new BufferedWriter(new FileWriter(wordcountFile))
    for ((w, c) <- rankedCounts)
      writer.write(w + ":" + c + "\n")
    writer.flush
    writer.close
  }
}

object BlockFollowers extends TwitterInstance with RateChecker {

  def main(args: Array[String]) {

    val settings = twitter.getAccountSettings
    val rateLimitStatus = settings.getRateLimitStatus
    println("remaining " + rateLimitStatus.getRemaining)
    println("seconds until reset " + rateLimitStatus.getSecondsUntilReset);
    println("settings: " + settings.getScreenName);

    val screenName = args(0)
    val followerIds = twitter.getFollowersIDs(screenName, -1).getIDs
    val candidatesIDs = followerIds.toList.drop(200).take(1000)
    
    println("candidates total: " + candidatesIDs.length)
//    val users = followerIds.map { x => twitter.showUser(x) }
    val candidateId = candidatesIDs(0)
    val user = twitter.showUser(candidateId)
    println(user.getName)
  }

}