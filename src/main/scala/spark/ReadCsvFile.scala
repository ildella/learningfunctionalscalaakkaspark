import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object ReadCsvFile {
  def main(args: Array[String]) {
  	println("starting")
    val conf = new SparkConf().setAppName("Simple Application")
    val sc = new SparkContext(conf)

    val lines = sc.textFile("data.csv")
	  println("file: %s".format(lines))
  	val pairs = lines.map(s => (s, 1))
  	// until here, data is not loaded, there are only pointers

  	val counts = pairs.reduceByKey((a, b) => a + b)
  	var array = counts.sortByKey().collect()
  	println("counts: %s".format(counts))
  	println("lines: %s".format(array.length))
  	println("lines 0: %s".format(array{0}))
  	println("lines 1: %s".format(array{1}))
  	println("lines 2: %s".format(array{2}))

  }
}
