import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object ReadSimpleTextFile {
  def main(args: Array[String]) {
  	execute()
  }

  def execute() {
    println("starting")
    val conf = new SparkConf().setAppName("ReadSimpleTextFile App").setMaster("local[*]")
    val sc = new SparkContext(conf)
    // val lines = sc.textFile("/home/ildella/Dropbox/Public/data.txt") // works!
    val lines = sc.textFile("data.txt")
    println("file: %s".format(lines))
    val lineLengths = lines.map(s => s.length)
    val pairs = lines.map(s => (s, 1))
    // until here, data is not loaded, there are only pointers
    val totalLength = lineLengths.reduce((a, b) => a + b)
    val counts = pairs.reduceByKey((a, b) => a + b)
    println("totalLength: %s".format(totalLength))
    var array = counts.sortByKey().collect()
    println("counts: %s".format(counts))
    println("array: %s".format(array.length))
    println("array 0: %s".format(array{0}))
    println("array 1: %s".format(array{1}))
    println("array 2: %s".format(array{2}))
    println("array 3: %s".format(array{3}))
    println("array 4: %s".format(array{4}))
  }
}
