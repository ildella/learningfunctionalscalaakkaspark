import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object ScanFolder {
  def main(args: Array[String]) {
    println("starting")
    val conf = new SparkConf().setAppName("Simple Application")
    val sc = new SparkContext(conf)
    val lines = sc.wholeTextFiles("/home/ildella/Music")
    println("file: %s".format(lines))
  }
}
