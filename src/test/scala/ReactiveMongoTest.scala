import org.scalatest.FunSuite

import scala.collection.JavaConversions._
import reactivemongo.bson._
import play.api.libs.iteratee.Iteratee
import reactivemongo.api._
import reactivemongo.api.collections.default._
import scala.concurrent.ExecutionContext.Implicits.global

class ReactiveMongoTest extends FunSuite {

  object BlockFollowers {

    def connect(): BSONCollection = {
      import reactivemongo.api._
      import scala.concurrent.ExecutionContext.Implicits.global

      // gets an instance of the driver
      // (creates an actor system)
      val driver = new MongoDriver
      val connection = driver.connection(List("ds031892.mongolab.com:31892"))

      val db = connection("untweeps")

      // By default, you get a BSONCollection.
      val collection = db("followers")
      collection
    }

    def read() {
      val collection = connect()
      val query = BSONDocument()
      val cursor = collection.find(query).cursor[BSONDocument]
      cursor.enumerate().apply(Iteratee.foreach { doc =>
        println("found document: " + BSONDocument.pretty(doc))
      })
    }

    def write() {
      val collection = connect()
      val doc = BSONDocument("p1" -> "v1")
      val cursor = collection.save(doc)
    }

  }

  test("An empty Set should have size 0") {
    //    BlockFollowers.printCandidatesIDs()
    BlockFollowers.write()
    BlockFollowers.read()
    //    assert(Set.empty.size == 1)
  }

}