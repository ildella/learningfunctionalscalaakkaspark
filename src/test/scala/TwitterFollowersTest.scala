import org.scalatest.FunSuite
import scala.collection.JavaConversions._

class TwitterFollowers extends FunSuite {

  import twitter4j._
  import scala.collection.JavaConversions._

  trait TwitterInstance {
    val twitter = new TwitterFactory().getInstance
  }

  trait RateChecker {

    def checkAndWait(response: TwitterResponse, verbose: Boolean = false) {
      val rateLimitStatus = response.getRateLimitStatus
      if (verbose) println("RLS: " + rateLimitStatus)

      if (rateLimitStatus != null && rateLimitStatus.getRemaining == 0) {
        println("*** You hit your rate limit. ***")
        val waitTime = rateLimitStatus.getSecondsUntilReset + 10
        println("Waiting " + waitTime + " seconds ( " + waitTime / 60.0 + " minutes) for rate limit reset.")
        Thread.sleep(waitTime * 1000)
      }
    }

  }

  object BlockFollowers extends TwitterInstance with RateChecker {

    
    def printCandidatesIDs() {

      val settings = twitter.getAccountSettings
      val rateLimitStatus = settings.getRateLimitStatus
      println("remaining " + rateLimitStatus.getRemaining)
      println("seconds until reset " + rateLimitStatus.getSecondsUntilReset);
      println("settings: " + settings.getScreenName);

      val screenName = "ildella"
      val followerIds = twitter.getFollowersIDs(screenName, -1).getIDs
      val candidatesIDs = followerIds.toList.drop(200).take(1000)

      println("candidates total: " + candidatesIDs.length)
      println(s"${candidatesIDs} ")

    }

    def writeCandidate() {
      val candidateId = 602207255
      val user = twitter.showUser(candidateId)
      println(user)
    }
   

  }

  test("An empty Set should have size 0") {
    //    BlockFollowers.printCandidatesIDs()
    BlockFollowers.writeCandidate()
    //    assert(Set.empty.size == 1)
  }

}