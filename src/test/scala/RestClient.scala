package rest

// import spray.can.Http
// import spray.routing._
// import spray.http._; import MediaTypes._
import org.json4s._
import org.json4s.jackson.Serialization.formats
// import spray.httpx.Json4sJacksonSupport

// import akka.actor.Actor._
// import akka.actor.Props._
// import akka.event.Logging._
import akka.http.model._
// import akka.http.model.headers._
// import akka.util._

import com.typesafe.config.{ Config, ConfigFactory }
import scala.util.{ Failure, Success }
import akka.actor.ActorSystem
import akka.stream.ActorFlowMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import akka.http._

import org.scalatest.FunSuite

class AkkaHttpTest extends FunSuite {

  test("basic http request") {
    import HttpMethods._
    import StatusCodes._
 
    // construct simple GET request to `homeUri`
    val homeUri = Uri("http://google.com")
    val req = HttpRequest(GET , uri = homeUri)
    println(req)

    // // construct simple GET request to "/index" which is converted to Uri automatically
    // HttpRequest(GET, uri = "/index")
     
    // // construct simple POST request containing entity
    // val data = ByteString("abc")
    // HttpRequest(POST, uri = "/receive", entity = data)
     
  }

  // test("TestClient ") {
  //   val testConf: Config = ConfigFactory.parseString("""
  //   akka.loglevel = INFO
  //   akka.log-dead-letters = off
  //   """)
  //   implicit val system = ActorSystem("ServerTest", testConf)
  //   implicit val fm = ActorFlowMaterializer()
  //   import system.dispatcher

  //   val host = "spray.io"

  //   println(s"Fetching HTTP server version of host `$host` ...")

  //   val connection = Http().outgoingConnection(host)
  //   val result = Source.single(HttpRequest()).via(connection).runWith(Sink.head)

  //   result.map(_.header[headers.Server]) onComplete {
  //     case Success(res)   ⇒ println(s"$host is running ${res mkString ", "}")
  //     case Failure(error) ⇒ println(s"Error: $error")
  //   }
  //   result onComplete { _ ⇒ system.shutdown() }
  // }


}
