import org.scalatest.FunSuite
import scala.io.Source
import scala.util.Random
import java.io.File
import akka.event.Logging

import akka.actor._

class UploadActor extends Actor {
  val log = Logging(context.system, this)
  var greeting = ""

  def receive = {
    case "test" => log.info("TEST")
    case x => log.info("NON TEST")
  }
}

class ParseCsvTest extends FunSuite {

  def post(s: String): String = {
    println(s"eseguo Post ... $s ...")
    if (Random.nextBoolean()) "201" else "500"
  }

  def transform(s: String): String = {
    s"$s TO JSON"
  }

  test("dream") {
    val lines = Source.fromFile("data.csv").getLines
    //     lines.foreach (x => post(transform(x)))
    val responses = lines.map(transform).map(post)
    //    responses.foreach(println)
    responses.map { x => (x, 1) }.foreach(println)
    //    val counts = responses.reduce((a, b) => a + b)
  }

  test("upload files with Akka") {

    val scan = ScanFolders
    val myBigFileArray = scan.recursiveListFiles(new File("."))
    val txts = myBigFileArray.filter(f => """.*\.txt$""".r.findFirstIn(f.getName).isDefined)
    val lines = txts

    case object Greet
    case class WhoToGreet(who: String)
    case class Greeting(message: String)

    val context = ActorSystem("MySystem")
    val greeter = context.actorOf(Props[Greeter], "greeter")
    val uploadActor = context.actorOf(Props[UploadActor], name = "upload")

        uploadActor ! "file:///baseFolder"

  }

}