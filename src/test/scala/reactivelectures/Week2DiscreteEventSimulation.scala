package week2

import org.scalatest.FunSuite

class WireSimulationTest extends FunSuite {
/*
  trait Simulation {
    type Action = () => Unit
    case class Event(time: Int, action: Action)
    private type Agenda = List[Event]
    private var agenda: Agenda = List()
    private var curtime = 0
    def currentTime: Int = curtime
    def afterDelay(delay: Int)(block: => Unit): Unit = {
      val item = Event(currentTime + delay, () => block)
      agenda = insert(agenda, item)
    }

    private def insert(ag: List[EVent], item: Event): List[Event] = ag match {
      case first :: rest if first.time <= item.time => 
        first :: insert (rest, item)
      case _ => 
        item :: ag
    }
    
    private def loop(): Unite = agenda match {
      case first :: rest => 
        agenda = rest
        curtime = first.time
        first.action()
        loop()
      case Nil =>  
    }
    

    def run(): Unit = {
      afterDelay(0) {
        println("*** simulation started @ "+currentTime+" ***")
      }
      loop()
    }

    def probe(name: String, wire: Wire): Unit = {
      def probeAction(): Unit = {
        println(s"$name $currentTime value = ${wire.getSignal}")
      }
      wire addAction probeAction
    }
  }

  trait Parameters {
    InverterDelay = 2
    AndGateDelay = 3
    OrGateDelay = 5
  }

  object sime extends Circuits with Parameters {
    
  }

  class Wire {
    private var sigVal = false
    private var actions: List[Action] = List()
    def getSignal: Boolean = sigVal
    def setSignal(s: Boolean): Unit = {
      if (s != sigVal) {
        sigVal = s
        actions foreach (_())
      }
    }
  }

  def inverter(input: Wire, output: Wire): Unit = {
    def invertAction(): Unit = {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output setSignal !inputSig}
    }
    input addAction invertAction
  }

  def andGate(in1: Wire, in2: Wire, output: Wire): Unit = {
    def andAction(): Unit = {
      val in1Sig = in1.getSignal
      val in2Sig = in2.getSignal
      afterDelay(AndGateDelay) { output setSignal (in1Sig & in2Sig)}
    }
    in1 addAction andAction
    in2 addAction andAction
  } 


  def orGate(in1: Wire, in2: Wire, output: Wire): Unit = {
    def orAction(): Unit = {
      val in1Sig = in1.getSignal
      val in2Sig = in2.getSignal
      afterDelay(OrGateDelay) { output setSignal (in1Sig | in2Sig)}
    }
    in1 addAction andAction
    in2 addAction andAction
  } 
*/
}