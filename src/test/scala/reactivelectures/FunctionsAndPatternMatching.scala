import java.io._
import org.scalatest.FunSuite

class JsonExamples extends FunSuite {

  abstract class JSON
  case class JSeq (elems: List[JSON]) extends JSON
  case class JObj (bindings: Map[String, JSON]) extends JSON
  case class JNum (num: Double) extends JSON
  case class JStr (str: String) extends JSON
  case class JBool (b: Boolean) extends JSON
  case class JNull extends JSON

  test("pattern matching recap") {
    // { case "ping" => "pong" } missing parameter type for expanded function 
    val f: String => String = { case "ping" => "pong" }
    assert("pong" == f("ping"))
  }


  test("partial function recap") {
    // PArtialFunction is a subtype of Function
    val f: PartialFunction[String, String] = { case "ping" => "pong"}
    assert(true == f.isDefinedAt("ping"))
    assert(false == f.isDefinedAt("pong"))
  }
}