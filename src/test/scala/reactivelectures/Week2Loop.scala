import org.scalatest.FunSuite

class LoopExamples extends FunSuite {

  // excercise
  def REPEAT (command: => Unit)(condition: => Boolean) = {
    // pseudo code:
    /* command
       if (condition) 
       else REPEAT (command) (condition)
    */
  }

  // actual scala syntax - For-Loops

}