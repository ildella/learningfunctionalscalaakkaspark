import java.io._
import org.scalatest.FunSuite

class ScanFoldersTest extends FunSuite {

  test("scan folder tree") {
    val scan = ScanFolders
    //scan.getFileTree(new File("c:\\main_dir")).filter(_.getName.endsWith(".scala")).foreach(println)
    val myBigFileArray = scan.recursiveListFiles(new File("."))
    assert(myBigFileArray.size > 1300)
    assert(myBigFileArray.size < 10000)

    val txts = myBigFileArray.filter(f => """.*\.txt$""".r.findFirstIn(f.getName).isDefined)
    val csv = myBigFileArray.filter(f => """.*\.csv$""".r.findFirstIn(f.getName).isDefined)
    // println(format("txts: %s", txts.size))
    assert(1 == txts.size)
    assert(1 == csv.size)
    val map = txts.map(f => f.getPath)
    // csv.foreach(x => println(x))
  }

  test("load CSV to REST API") {
    
  }


}