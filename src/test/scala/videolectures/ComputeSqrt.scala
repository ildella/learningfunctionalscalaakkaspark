import org.scalatest.FunSuite

class DayOneComputeSqrt extends FunSuite {

	def abs(x: Double) = if (x > 0) x else -x

	def isCloseEnough(guess: Double, x: Double) = if (abs(guess - x) < 0.001) true else false

	def isGoodEnough(guess: Double, x: Double) = isCloseEnough(guess * guess, x)

	def improve(guess: Double, x: Double) = (guess + x / guess) / 2

	def sqrtIter(guess: Double, x: Double): Double = 
		if (isGoodEnough(guess, x)) guess
		else sqrtIter(improve(guess, x), x)

	def sqrt(x: Double) = sqrtIter(1, x)

	test("abs") {
		assert(5 == abs(-5))
		assert(5 == abs(5))
		assert(1 == abs(2-1))
		assert(1 == abs(1-2))
	}

	test("isGoodEnough") {
		assert(false == isGoodEnough(1, 4))
		assert(true == isGoodEnough(1.9999, 4))
	}

	test("isCloseEnough") {
		assert(false == isCloseEnough(1, 2))
		assert(false == isCloseEnough(1, 1.02))
		assert(true == isCloseEnough(1, 1.0001))
	}

	test("sqrt") {
		sqrt(0.0001)
		sqrt(0.1e-20)
		sqrt(1.0e20)
		//assert(2 == sqrt(4))
		//assert(3 == sqrt(9))
		//assert(1.4142 == sqrt(2))
	
	}


}