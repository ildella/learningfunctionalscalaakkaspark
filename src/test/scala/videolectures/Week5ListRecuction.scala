package week5

import org.scalatest.FunSuite

class ListReductionTest extends FunSuite {

  // typical combination operation: sum
  def sum(xs: List[Int]): Int = xs match {
    case Nil => 0
    case y :: ys => y + sum(ys) 
  }

  test("reduction - sum") {
    assert(100 == sum(List(30, 30, 30, 10)))
  }

}