import org.scalatest.FunSuite
import scala.collection.JavaConversions._
import java.io._

class HighOrder extends FunSuite {

  def cube(x: Int) = x * x * x
  def id(x: Int) = x

  def sumCubes(a: Int, b: Int): Int = 
    if (a > b) 0 
    else cube(a) + sumCubes(a + 1, b)

  test("sum cubes") {
    assert(27 == cube(3))
    assert(1+8+27+64 == sumCubes(1,4))
  } 

  def sum(f: Int => Int, a: Int, b: Int): Int = 
    if (a > b) 0
    else f(a) + sum(f, a + 1, b)

  test("high order one") {
    assert(100 == sum(cube, 1, 4))
  }

    // (x: Int) PARAMETER
  // x*x*x BODY
  (x: Int) => x * x * x

  test("anonymouse cubes sum") {
    assert(100 == sum(x => x*x*x, 1, 4))    
  }

  // funtion returning another function
   def sum(f: Int => Int): (Int, Int) => Int = {
      def sumF(a: Int, b: Int): Int = {
        if (a > b) 0
        else f(a) + sumF(a + 1, b)
      }
      sumF
    }

  
  def sumCubes2 = sum(x => x*x*x)  

  test("sum cubes with function returning function") {
    assert(100 == sumCubes2(1, 4))
    assert(100 == sum (cube) (1, 4)) // avoid middleman
  }

  
  def product (f: Int => Int) (a: Int, b: Int): Int = 
    if (a > b) 1
    else f(a) * product(f)(a + 1, b)

  test("product") {
    assert(144 == product(x => x * x)(3, 4))
  }

  def generic (f: Int => Int) (a: Int, b: Int): Int = 
    if (a > b) 1
    else f(a) * product(f)(a + 1, b)

}

class Rational (x: Int, y: Int) {

  require(y != 0, "denominator cannot be zero")

  def this(x: Int) = this(x, 1)

   def n = x
   def d = y 

  def add(that: Rational) = 
    new Rational(n * that.d + d * that.n, d * that.d)

  override def toString = n + "/" + d

}

class Data extends FunSuite {

  test("fun with rationals") {
    def r = new Rational(3,4)
    assert(3 == r.n)
    def y = new Rational(5,7)
    assert("5/7" == y.toString)
  }

  test("preconditions") {
    def strange = new Rational(1, 1)
    val result = strange.add(strange) // need a test to verify exception thrown
    def secondContructor = new Rational(6)
  }
  
}