package week5

import org.scalatest.FunSuite

// lists are immutable, array are mutable
// 
class ListTest extends FunSuite {

  val nums = 1 :: 2 :: 3 :: Nil
  assert(List(1, 2, 3) == nums)
  // head constant time
  assert(1 == nums.head)
  val fruit = "apples" :: "oranges" :: Nil
  assert("oranges" == fruit.tail.head)
  val diag3 = List(1, 0, 0) :: List(0, 1, 0) :: List(0, 0, 1)
  assert(List(1, 0, 0) == diag3.head)

  // week 5
  assert(3 == nums.length)
  // last time proportional to list length!
  assert("oranges" == fruit.last)
  assert(2 == nums(1))
  assert(List(1, 2) == nums.take(2))
  assert(Nil == nums.take(0))
  assert(List(2, 3) == nums.drop(1))
  assert(List(3, 2, 1) == nums.reverse)
  assert(List(1, 2, 3, "apples", "oranges") == nums ++ fruit)
  val numsUpdated = nums updated (1, 99)
  assert(List(1, 99, 3) == numsUpdated)
  assert(1 == numsUpdated.indexOf(99))
  assert(false == numsUpdated.contains(8))
  
  val sums = nums.map(x => x+1)
  assert(2 == sums.head)
  assert(1 == sums.indexOf(3))
  assert(true == sums.contains(4))
  assert(false == sums.contains(1))

}