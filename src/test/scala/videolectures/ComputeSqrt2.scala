import org.scalatest.FunSuite

class DayOneComputeSqrtWithBlock extends FunSuite {

  def sqrt(x: Double) = {

    def abs(x: Double) = if (x > 0) x else -x

    def isCloseEnough(guess: Double) = if (abs(guess - x) < 0.001) true else false

    def isGoodEnough(guess: Double) = isCloseEnough(guess * guess)

    def improve(guess: Double) = (guess + x / guess) / 2

    def sqrtIter(guess: Double): Double = 
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    sqrtIter(1)

  }

  
  test("sqrt with block") {
    sqrt(0.0001)
    sqrt(0.1e-20)
    sqrt(1.0e20)
    //assert(2 == sqrt(4))
    //assert(3 == sqrt(9))
    //assert(1.4142 == sqrt(2))
  
  }


}