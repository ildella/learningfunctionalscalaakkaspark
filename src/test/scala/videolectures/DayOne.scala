import org.scalatest.FunSuite
import scala.collection.JavaConversions._
import java.io._

class DayOne extends FunSuite {

  def square(x: Double) = x * x

  def sumOfSquare(x: Double, y: Double) = square(x) + square(y)

  def and(x: Boolean, y: Boolean) = if (x) y else false // X && y almost equivalent perche' se gli passi un loop sulla y essendo passata per Value la calcola subito e va in loop
  //def and(x: Boolean, y => Boolean) = if (x) y else false // X && y equivalent perche' e' by name 

  class Person (val name: String, val age: Int)

  // bloks
  def sumOfSquare2(x: Double, y: Double) = {

    def square(x: Double) = x * x

    square(x) + square(y)

  }

  test("square") {
    assert(4 == square(2))
    assert(18 == sumOfSquare(3, 3))
    assert(18 == sumOfSquare2(3, 3))
  }
  
  test("partition sample 1") {
    val persons: Array[Person] = Array()
    val p1 = new Person("Pino", 19)
    val p2 = new Person("Gino", 1)
    val p3 = new Person("Lino", 49)
    val p4 = new Person("Tino", 39)
    val p5 = new Person("Rino", 29)
    val p6 = new Person("Pino", 9)
    val people: Array[Person] = Array(p1, p2, p3, p4, p5, p6)
    val (minors, adults) = people.par partition (_.age < 18)
    assert(2 == minors.size)
    assert(4 == adults.size)
    // val (minors, adults) = people.par partition (_.age < 18)
  }

  def f(p: Person) = p.age
  def g(p: Person) = p.name + "." + p.age

  test("custom function sample 1") {
    val p1 = new Person("Pino", 19)
    val p2 = new Person("Gino", 1)
    val p3 = new Person("Lino", 49)
    val p4 = new Person("Tino", 39)
    val p5 = new Person("Rino", 29)
    val p6 = new Person("Pino", 9)
    
    val people: Array[Person] = Array(p1, p2, p3, p4, p5, p6)
    val aggregateInt = people.map(x => x.age)
    assert(6 == aggregateInt.size)
    assert(19 == aggregateInt{0})
    
    val aggregateInt2 = people.map(f)
    assert(19 == aggregateInt2{0})

    val aggregateCombine = people.map(g)
    assert("Pino.19" == aggregateCombine{0})


  }


}