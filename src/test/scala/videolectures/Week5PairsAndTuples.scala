package week5

import org.scalatest.FunSuite

// lists are immutable, array are mutable

class PairsTuplesTest extends FunSuite {

  val pair = ("answer", 42)
  assert("answer" == pair._1)
  assert(42 == pair._2)

  val tuple = ("answer", 42, "another answer", 18)
  assert("another answer" == tuple._3)
  assert(18 == tuple._4)

 def merge(xs: List[Int], ys: List[Int]): List[Int] =
    (xs, ys) match {
      case (Nil, ys) => Nil
      case (xs, Nil) => Nil
      case (x :: xs1, y :: ys1) => 
        if (x < y) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
    }

  val merged = merge(List(1,3,9), List(2,4,6))   
  assert(List() == merge(List(), List()))
  assert(List() == merge(List(), List(1)))
  assert(List() == merge(List(2,3,4), List()))
  assert(List(1, 2, 3, 4, 6) == merged) // ummmmm...

}
